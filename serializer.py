
"""
Examples:

key,value iterator of dict

for key,value in vars(t).items():
  print(x)
  print(y)

#where t is a class object

json.dumps({key:value for key, value in vars(testclass).items() if not key.startswith('__') and not callable(key)})

"""
import json
import pdb
import typing

class anonymous:
  def __eq__(self, other):
    if not isinstance(other,anonymous):
      return NotImplemented
    
    return self.__dict__ == other.__dict__
   
_ = anonymous
   
class serializer(object):
  def __init__(self):
    pass

    
  def isValueType(obj):
    types = [int, float, bool, str, complex]
    return any([hasattr(obj, "__name__") and obj.__name__ == t.__name__ or isinstance(obj, t) for t in types])

    
  def getAttributes(obj, depth=0):
    name = type(obj).__name__ if not hasattr(obj, "__name__") else obj.__name__
    #print("Depth: {}, obj=({}) {}".format(depth, name, str(obj)))

    if serializer.isValueType(obj):
      return obj
    elif isinstance(obj, list):
      return [serializer.getAttributes(x, depth+1) for x in obj]
    elif isinstance(obj, typing.GenericMeta) and obj.__name__ == 'List':
      return obj
    elif isinstance(obj, typing.GenericMeta) and obj.__name__ == 'Dict':
      return obj
    elif isinstance(obj, dict):
      return { key: serializer.getAttributes(value, depth+1) for key,value in obj.items() } #dict
    elif isinstance(obj, set):
      return { serializer.getAttributes(x, depth+1) for x in obj } #set
    elif hasattr(obj, "__name__") and hasattr(obj, "__annotations__"):
      return { key:serializer.getAttributes(value, depth+1) for key, value in obj.__annotations__.items() if not key.startswith('__') and not callable(key) }
    elif hasattr(obj, "__dict__"):
      return { key:serializer.getAttributes(value, depth+1) for key, value in obj.__dict__.items() if not key.startswith('__') and not callable(key) }

    raise ValueError("Cannot determine the type of 'obj': {}".format(name))

    
  def setAttributes(data_obj, def_obj, transfer_unknown_properties = False, depth = 0):
    #prefix = " - " * depth
    #data_name = type(data_obj).__name__ if not hasattr(data_obj, "__name__") else data_obj.__name__
    #def_name = type(def_obj).__name__ if not hasattr(def_obj, "__name__") else def_obj.__name__
    #print(" :: " + prefix + "data_obj={}, def_obj={}".format(data_name, def_name))
    pdb.set_trace()
    # special List type
    if hasattr(def_obj, "__name__") and def_obj.__name__ == 'List':
      #print(" ll " + prefix + " list type ")
      list_type = def_obj.__args__[0]
      return [serializer.setAttributes(x, list_type, transfer_unknown_properties, depth+1) for x in data_obj]

    # special Dict type
    if hasattr(def_obj, "__name__") and def_obj.__name__ == 'Dict':
      #print(" dd " + prefix + " dict type ")
      dict_type = def_obj.__args__[1]
      return { key:serializer.setAttributes(value, dict_type, transfer_unknown_properties, depth+1) for key,value in data_obj.items() }

    # value type
    if serializer.isValueType(def_obj):
      #print(" vv " + prefix + " value type ")
      return def_obj(data_obj)

    # here, we are working with an object with { key : value } pairs
    class_attr = serializer.getAttributes(def_obj)
    if dict() == class_attr and transfer_unknown_properties is True:
      class_attr = serializer.getAttributes(data_obj)    
      ret_obj = _()
    else:
      ret_obj = def_obj()
      
    for key, subclass_def in class_attr.items():
      #print(" == " + prefix + str(key) + " :: " + str(subclass_def))
      value = data_obj[key]
      setattr(ret_obj, key, serializer.setAttributes(value, subclass_def, transfer_unknown_properties, depth+1))     
    
    return ret_obj
	
  
  def serialize(obj):
    return json.dumps(serializer.getAttributes(obj))

    
  def deserialize(data, class_obj = None):
    data_dict = json.loads(data)
    
    if class_obj is not None:
      return serializer._deserialize_with_definition(data_dict, class_obj)

    return serializer._deserialize_without_definition(data_dict)

    
  def _deserialize_without_definition(data_dict):
    c1 = _()
    return serializer.setAttributes(data_dict, _, True)

    
  def _deserialize_with_definition(data_dict, class_obj):
    if not callable(class_obj):
      raise TypeError("Could not instantiate a new object of type '{}'".format(name))

    return serializer.setAttributes(data_dict, class_obj)


