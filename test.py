import unittest
import pdb
from serializer import serializer as s
from serializer import anonymous as _
from test_classes import *

# class TestSerializing(unittest.TestCase):
  # def setup(self):
    # pass

  # def teardown(self):
    # pass 

  # def test_int(self):
    # self.assertEqual(s.serialize(5), '5')

  # def test_str(self):
    # self.assertEqual(s.serialize("hey"), '"hey"')

  # def test_simpleobject(self):
    # expected = '{"a": 5}'
    # obj = _()
    # obj.a = 5
    # actual = s.serialize(obj)
    # self.assertEqual(actual, expected)

  # def test_list(self):
    # l = [1,4,6,7,8,9]
    # self.assertEqual(s.serialize(l), '[1, 4, 6, 7, 8, 9]')

  # def test_dict(self):
    # d = { "a": 5, "b": 'hey'}
    # self.assertEqual(s.serialize(d), '{"a": 5, "b": "hey"}')

# class TestDeserializing(unittest.TestCase):
  # def setup(self): pass
  # def teardown(self): pass

  # def test_int(self):
    # self.assertEqual(s.deserialize("5", int), 5)

# class TestComplex(unittest.TestCase):
  # def test_complex_case(self):
    # expected = Create()
    # data_obj, def_obj = PrepareTest()  
    # json = s.serialize(data_obj)
    # actual = s.deserialize(json, def_obj)
    # self.assertEqual(actual, expected)

class TestNoDefinition(unittest.TestCase):
  def test_no_definition(self):
    c1 = _()
    c1.a = "a"
    c1.b = 3
    data = s.serialize(c1)
    c1_after = s.deserialize(data)    
    self.assertEqual(c1 == c1_after, True)
	

if __name__ == '__main__':
  unittest.main()
