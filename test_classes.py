from typing import List,Dict 
import json 
from serializer import serializer as s

class Model(object):
  def __eq__(self, other: object):
    return self.__dict__ == other.__dict__

class C(Model):
  id: int
  masterplan: str
  number_of_tries: int
  rating: float

class A(Model):
  id: int 
  name: str
  dictofC: Dict[str,C]

class B(Model):
  id: int
  description: str
  listofA: List[A]

def Create():
  c1 = C()
  c1.id = 100
  c1.masterplan = "Take over the world"
  c1.number_of_tries = 99343
  c1.rating = 0.00001
  c2 = C()
  c2.id = 101
  c2.masterplan = "Have a beer on friday!"
  c2.number_of_tries = 11
  c2.rating = 100.0
  c3 = C()
  c3.id = 102
  c3.masterplan = "Win over Kasparov in Chess"
  c3.number_of_tries = 103
  c3.rating = 13.8901
  a1 = A()
  a1.id = 1
  a1.name = "Daniel"
  a1.dictofC = { "first":c1, "second":c2 }
  a2 = A()
  a2.id = 2
  a2.name = "Philip"
  a2.dictofC = { "third" : c3 }
  b = B()
  b.id = 10
  b.description = "A long list of A's"
  b.listofA = [ a1, a2 ]
  return b

def PrepareTest():
  def_obj = B
  data_obj = json.loads(s.serialize(Create()))
  return (data_obj, def_obj)  
